#include "terminal6502.h"
#include "mem.h"

#include <config.h>

#ifndef _BSD_SOURCE
#define _BSD_SOURCE
#endif

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

extern char *optarg;

void usage() {
    fprintf(stderr, "terminal6502 OPTIONS\n"
            "Options:\n"
            "\t-r <rom>\n"
            "\t-o <offset|0>\n"
            "\t-p <program counter|default>\n"
            "\t-c <max_cycles|0>\n");
}

int main(int argc, char * const argv[])
{
    struct terminal6502 terminalMachine;
    const char* rom_path = SHARE "/terminal6502.bin";
    uint16_t offset = 0;
    uint32_t max_cycles = 0;

    uint16_t pc = 0;
    uint8_t isPcGiven = 0;
    uint8_t isMemDumpRequested = 0;

    int opt = 0;

    while (-1 != (opt = getopt(argc, argv, "r:ho:c:p:m"))) {

        switch (opt) {
            case 'r':
                rom_path = strdup(optarg);
                break;

            case 'o':
                offset = atoi(optarg);
                offset = strtoul(optarg, NULL, 10);
                break;

            case 'c':
                max_cycles = strtoul(optarg, NULL, 10);
                break;

            case 'm':
                isMemDumpRequested = 1;
                break;

            case 'p':
                pc = strtoul(optarg, NULL, 10);
                isPcGiven = 1;
                break;

            default:
            case 'h':
                usage();
                exit(1);
                break;
        }
    }

    // initialize the machine and peripherals
    terminal6502_init(&terminalMachine);

    // load memory
    fprintf(stdout, "Loading ROM: %s @ %04x\n", rom_path, offset);
    if (!terminal6502_load_rom(&terminalMachine, rom_path, offset)) {
        fprintf(stderr, "Unable to load rom: %s\n", rom_path);
        return 1;
    }

    // perform initial reset
    terminal6502_reset(&terminalMachine);

    // adjust the pc if requested
    if (isPcGiven) {
        terminal6502_set_pc(&terminalMachine, pc);
    }

    // run program
    fprintf(stdout,
            "Starting program at: %04x, max_cycles %d (0 = unlimited) ...\n",
            terminalMachine.cpu.regs.pc,
            max_cycles);

    int rv = terminal6502_run(&terminalMachine, max_cycles);

    // dump memory if requested
    if (isMemDumpRequested) {
        mem_dump(&terminalMachine.memory);
    }

    return rv;
}
