# Terminal 6502

This is a simple concept 6502 machine. It provides 64 kB or RAM and a
serial output device which prints to *STDOUT*. Mostly useful for concept
proofing and testing.
