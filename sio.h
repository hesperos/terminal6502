#ifndef SIO_H_
#define SIO_H_

#include <stdint.h>

/// In this mode, the sio device will output alpha numeric characters
#define SIO_MODE_CHAR 0

/// In this mode, the sio device will output alpha hexadecimal numbers
#define SIO_MODE_HEX 1

/// In this mode, the sio device will output alpha decimal numbers
#define SIO_MODE_DEC 2

/// In this mode, the sio device will output alpha unsigned decimal numbers
#define SIO_MODE_UNS 3

/// sio reception flag, 1 - ready
#define SIO_RX_READY 0

/// sio reception flag, 1 - ready, 0 - busy
#define SIO_TX_READY 1

/// sio output mode selection
#define SIO_MODE0 2
#define SIO_MODE1 3

/**
 * @brief Serial IO device context
 */
struct sio {
    uint8_t status;
};

/**
 * @brief Initialize the device
 *
 * @param sio
 */
void sio_init(struct sio* sio);

/**
 * @brief Load byte into transmission buffer
 *
 * @param data
 */
void sio_tx(struct sio* sio, uint8_t data);

/**
 * @brief Get byte from the receiver buffer
 *
 * @return byte of data
 */
uint8_t sio_rx(struct sio* sio);

/**
 * @brief Get status of SIO device
 *
 * @return status byte
 */
uint8_t sio_get_status(struct sio* sio);

/**
 * @brief Set status register of SIO device
 *
 * Any status set doesn't affet the first two status bits
 *
 * @param status new status
 */
void sio_set_status(struct sio* sio, uint8_t status);

#endif /* SIO_H_ */

