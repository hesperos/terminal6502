#ifndef MAPPER_H_
#define MAPPER_H_

#include <stdint.h>

typedef uint8_t (*mapper_load_t)(uint16_t, void* user_data);
typedef void (*mapper_store_t)(uint16_t, uint8_t data, void* user_data);

/**
 * @brief Mapping list
 *
 * This forms reversed linked list of memory range mappings. A range is defined
 * by its ending address (i.e if a range has ending address 0x1000, it maps all
 * addresses from 0x1000 down to 0x0000, or subsequent range).
 *
 * If there are two entries, one with ending address 0x1000 and subsequent one
 * with ending address 0x500, then first mapping maps all addresses from 0x1000
 * (inclusively) to 0x501, and second one from 0x500 to 0x00.
 */
struct mapper_mapping {
    uint16_t end;
    mapper_load_t load;
    mapper_store_t store;
    void* user_data;
    struct mapper_mapping *next;
};

/**
 * @brief Mapper device context
 */
struct mapper {
    struct mapper_mapping *mappings;
};

/**
 * @brief Initialize the mapping device
 *
 * @param mapper
 */
void mapper_init(struct mapper* mapper);

/**
 * @brief Load data from given location
 *
 * @return data
 */
uint8_t mapper_load(struct mapper* mapper, uint16_t address);

/**
 * @brief Store data at given location
 *
 * @param address
 * @param data
 */
void mapper_store(struct mapper* mapper, uint16_t address, uint8_t data);

/**
 * @brief Map given region to provided io
 *
 * @param end region end
 * @param load load routine
 * @param store store routine
 */
void mapper_add_mapping(
        struct mapper* mapper,
        uint16_t end,
        mapper_load_t load,
        mapper_store_t store,
        void* user_data);

#endif /* MAPPER_H_ */
