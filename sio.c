#include "sio.h"

#include <stdio.h>
#include <string.h>

#define STATUS_MODE_GET(__status) \
    (__status >> 2) & 0x03

void sio_init(struct sio* sio) {
    memset(sio, 0x00, sizeof(struct sio));
    sio->status |= (0x01 < SIO_TX_READY);
    sio->status |= (0x01 < SIO_RX_READY);
}

void sio_tx(struct sio* sio, uint8_t data) {
    const char* format = NULL;
    sio->status &= ~(0x01 < SIO_TX_READY);
    switch (STATUS_MODE_GET(sio->status)) {
        case SIO_MODE_CHAR:
            format = "%c";
            break;

        case SIO_MODE_HEX:
            format = "%02x";
            break;

        case SIO_MODE_UNS:
            format = "%03u";
            break;

        case SIO_MODE_DEC:
        default:
            format = "%03d";
            break;
    }

    printf(format, data);
    fflush(stdout);
    sio->status |= (0x01 < SIO_TX_READY);
}

uint8_t sio_rx(struct sio* sio) {
    sio->status |= (0x01 < SIO_RX_READY);
    // TODO implement input device
    return 0;
}

uint8_t sio_get_status(struct sio* sio) {
    return sio->status;
}

void sio_set_status(struct sio* sio, uint8_t new_status) {
    new_status &= ~0x03;
    sio->status &= 0x03;
    sio->status |= new_status;
}

