#include "terminal6502.h"

#include <stdio.h>

// wrappers for memory access for the CPU
static void terminal6502_store(
        uint16_t address, uint8_t data,
        void* user_data) {
    struct terminal6502* t = user_data;
    mapper_store(&t->mapper, address, data);
}

static uint8_t terminal6502_load(uint16_t address,
        void* user_data) {
    struct terminal6502* t = user_data;
    return mapper_load(&t->mapper, address);
}

// wrappers for memory access
static void terminal6502_mem_store(
        uint16_t address, uint8_t data,
        void* user_data) {
    struct terminal6502* t = user_data;
    mem_store(&t->memory, address, data);
}

static uint8_t terminal6502_mem_load(uint16_t address,
        void* user_data) {
    struct terminal6502* t = user_data;
    return mem_load(&t->memory, address);
}

// wrappers for SIO access
static void terminal6502_sio_store(
        uint16_t address, uint8_t data,
        void* user_data) {
    struct terminal6502* t = user_data;

    switch (address & 0x0f) {
        case 0x00:
            sio_tx(&t->sio, data);
            break;

        case 0x01:
            /* no write support */
            break;

        case 0x02:
            sio_set_status(&t->sio, data);
            break;

        default:
           break;
    }
}

static uint8_t terminal6502_sio_load(uint16_t address,
        void* user_data) {
    struct terminal6502* t = user_data;
    uint8_t data = 0x00;

    switch (address & 0x0f) {
        case 0x00:
            /* no read support */
            break;

        case 0x01:
            data = sio_rx(&t->sio);
            break;

        case 0x03:
            sio_get_status(&t->sio);
            break;

        default:
           break;
    }

    return mem_load(&t->memory, address);
}

// system setup routine
void terminal6502_init(struct terminal6502* terminal6502) {
    sio_init(&terminal6502->sio);
    mapper_init(&terminal6502->mapper);
    mem_init(&terminal6502->memory);

    cpu6502_init(&terminal6502->cpu,
            terminal6502_load,
            terminal6502_store,
            terminal6502);

    // high fragment of RAM
    mapper_add_mapping(&terminal6502->mapper,
            0xffff,
            terminal6502_mem_load,
            terminal6502_mem_store,
            terminal6502);

    // SIO
    mapper_add_mapping(&terminal6502->mapper,
            0x400f,
            terminal6502_sio_load,
            terminal6502_sio_store,
            terminal6502);

    // remaining RAM fragment of RAM
    mapper_add_mapping(&terminal6502->mapper,
            0x3fff,
            terminal6502_mem_load,
            terminal6502_mem_store,
            terminal6502);
}

int terminal6502_load_rom(struct terminal6502* terminal6502,
        const char* rom_path,
        uint16_t offset) {
    FILE *rom_file = fopen(rom_path, "rb");

    if (!rom_file) {
        return 0;
    }

    const size_t chunk = 1024;
    const size_t max_bytes = MEMORY_RAM_SIZE - offset;
    size_t read_bytes = 0;
    size_t read_total = 0;
    size_t to_read = chunk < max_bytes ? chunk : max_bytes;
    uint8_t* ptr = &terminal6502->memory.memory[offset];

    while (!feof(rom_file) &&
            (read_bytes = fread(ptr, 1, to_read, rom_file))) {
        read_total += read_bytes;
        ptr += read_bytes;
        if (read_total >= max_bytes) {
            break;
        }
        size_t mem_left = max_bytes - read_total;
        to_read = chunk < mem_left ? chunk : mem_left;
    }

    fclose(rom_file);
    fprintf(stdout, "Loaded: %u bytes\n", read_total);
    return 1;
}

void terminal6502_set_pc(struct terminal6502* terminal6502, uint16_t pc) {
    terminal6502->cpu.regs.pc = pc;
}

void terminal6502_reset(struct terminal6502* terminal6502) {
    cpu6502_reset(&terminal6502->cpu);
}

int terminal6502_run(struct terminal6502* terminal6502,
        uint32_t max_cycles) {

    for (;;) {
        cpu65x02_step(&terminal6502->cpu);
        if (max_cycles && terminal6502->cpu.cycles > max_cycles) {
            break;
        }
    }

    return 0;
}
