#ifndef MEM_H_
#define MEM_H_

#include <stdint.h>

#define MEMORY_RAM_SIZE (64 * 1024)

struct memory {
    uint8_t memory[MEMORY_RAM_SIZE];
};

void mem_init(struct memory* mem);
uint8_t mem_load(struct memory* mem, uint16_t address);
void mem_store(struct memory* mem, uint16_t address, uint8_t data);
void mem_dump(struct memory* mem);

#endif /* MEM_H_ */
