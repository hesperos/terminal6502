#include "mapper.h"

#include <stdlib.h>
#include <string.h>

#define NOVALUE 0xff

struct mapper_mapping* _mapping_make_node(uint16_t end,
        mapper_load_t load, mapper_store_t store,
        void* user_data) {
    struct mapper_mapping* node = malloc(sizeof(struct mapper_mapping));
    node->end = end;
    node->load = load;
    node->store = store;
    node->user_data = user_data;
    node->next = NULL;
    return node;
}

static void _mapping_insert(
        struct mapper* mapper,
        uint16_t end,
        mapper_load_t load,
        mapper_store_t store,
        void* user_data) {

    // first node, special case
    if (!mapper->mappings) {
        mapper->mappings =
            _mapping_make_node(end, load, store, user_data);
    }
    else {
        struct mapper_mapping* current = mapper->mappings;

        while (current->next && (current->end > end)) {
            current = current->next;
        }

        struct mapper_mapping* node =
            _mapping_make_node(end, load, store, user_data);
        node->next = current->next;
        current->next = node;
    }
}

static struct mapper_mapping* _mapping_find(struct mapper *mapper,
        uint16_t address) {
    struct mapper_mapping* m = mapper->mappings;

    while (m) {
        if (m->end >= address &&
                (!m->next || m->next->end < address)) {
            // we've got a match
            return m;
        }
        m = m->next;
    }

    return NULL;
}

uint8_t mapper_load(struct mapper* mapper, uint16_t address) {
    struct mapper_mapping* m = _mapping_find(mapper, address);
    return m ? m->load(address, m->user_data) : NOVALUE;
}

void mapper_store(struct mapper* mapper, uint16_t address, uint8_t data) {
    struct mapper_mapping* m = _mapping_find(mapper, address);
    if (m) {
        m->store(address, data, m->user_data);
    }
}

void mapper_add_mapping(
        struct mapper* mapper,
        uint16_t end,
        mapper_load_t load,
        mapper_store_t store,
        void* user_data) {
    _mapping_insert(mapper, end, load, store, user_data);
}

void mapper_init(struct mapper* mapper) {
    memset(mapper, 0x00, sizeof(struct mapper));
}
