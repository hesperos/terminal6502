#ifndef TERMINAL6502_H_
#define TERMINAL6502_H_

#include "sio.h"
#include "mapper.h"
#include "mem.h"

#include <cpu65x02/cpu65x02.h>

/*
 * +------------+ -+-             VECTORS:
 * |            |  | 0xffff       - 0xffff - IRQ high
 * |  VECTORS   |  |              - 0xfffe - IRQ low
 * |            |  | 0xfffa       - 0xfffd - RESET high
 * +------------+ -+-             - 0xfffc - RESET low
 * |            |  | 0xfff9       - 0xfffb - NMI high
 * |    ROM     |  |              - 0xfffa - NMI low
 * |            |  | 0x8000
 * +------------+ -+-             IO:
 * |            |  | 0x7fff       - 0x4002 - SIO STATUS
 * |    IO      |  |              - 0x4001 - SIO RX
 * |            |  | 0x4000       - 0x4000 - SIO TX
 * +------------+ -+-
 * |            |  | 0x3fff
 * |    RAM     |  |
 * |            |  | 0x0200
 * +------------+ -+-
 * |            |  | 0x01ff
 * |   STACK    |  |
 * |            |  | 0x0100
 * +------------+ -+-
 * |            |  | 0x00ff
 * |    RAM     |  |
 * |            |  | 0x0000
 * +------------+ -+-
 */

struct terminal6502 {
    struct cpu65x02 cpu;
    struct sio sio;
    struct mapper mapper;
    struct memory memory;
};

void terminal6502_init(struct terminal6502* terminal6502);
int terminal6502_load_rom(struct terminal6502* terminal6502,
        const char* rom_path,
        uint16_t offset);
void terminal6502_reset(struct terminal6502* terminal6502);
void terminal6502_set_pc(struct terminal6502* terminal6502, uint16_t pc);
int terminal6502_run(struct terminal6502* terminal6502, uint32_t max_cycles);

#endif /* TERMINAL6502_H_ */
