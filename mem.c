#include "mem.h"

#include <stdio.h>
#include <stdint.h>
#include <string.h>

uint8_t mem_load(struct memory* mem, uint16_t address) {
    return mem->memory[address % MEMORY_RAM_SIZE];
}

void mem_store(struct memory* mem, uint16_t address, uint8_t data) {
    mem->memory[address % MEMORY_RAM_SIZE] = data;
}

void mem_init(struct memory* mem) {
    memset(mem, 0x00, sizeof(struct memory));
}


void mem_dump(struct memory* mem) {
    size_t current = 0;
    size_t size = 32;
    const size_t bytes_per_line = 32;

    while (size--) {
        if (!(current%bytes_per_line)) {
            printf("%04x: ", (unsigned)current);
        }

        printf("%02x ", (unsigned)mem->memory[current++]);

        if (!(current%bytes_per_line)) {
            printf("\n");
        }
    }

    printf("\n");
    fflush(stdout);
}
